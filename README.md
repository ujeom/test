This is a project for testing the gitlab CICD for raspi.

We aime to use specific runner(runner on raspi) with shell to achieve that, when commit a change to the project, the project will be produce as a docker container and run on raspi.

Steps to achieve.

1. Install gitlab runner on raspi`sudo apt-get install gitlab-runner`.
2. Regist the runner as shell
`sudo gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token REGISTRATION_TOKEN \
  --executor shell \
  --description "My Runner"`
  where reigistration_token is the number that be founded under the gitlab menu-setting-CICD-Runners
3. Make sure the docker is avaliable on your raspi.
4. Add the gitlab-runner user to the docker group `sudo usermod -aG docker gitlab-runner`
5. Verify that gitlab-runner has access to Docke `sudo -u gitlab-runner -H docker info`
6. Verify in the setting menu that runner is reigistrated successfully.

After that you the CICD should work automatically after each commit with this project.

In case you are not maintaner or owner of this project, you can create your porject on gitlab and copy all the file.

add cross plateform feature
